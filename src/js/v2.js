let newSquare = new Square(0, 0, 'red');
let playField = document.querySelector('#playfield')

document.body.addEventListener('keydown', function (event) {
  newSquare.moveLeft(); // on utilise la methode pour bouger le carré
  console.log(newSquare);

  playField.innerHTML = ""; // qui sert a contenir le carré mais avant on la vide
  playField.appendChild(newSquare.draw());

})
